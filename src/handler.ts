import {
  APIGatewayProxyEvent,
  APIGatewayProxyHandler,
  Context,
} from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { createLogger } from 'winston';
import { StreamLimitController } from './controller/stream.limit.controller';
import { DynamoDbStreamRepository } from './repository/dynamodb/dynamodb.stream.repository';
import { StreamLimitService } from './service/stream.limit.service';
import { HttpStatusCode } from './utils/http.status.code';

export const stream: APIGatewayProxyHandler = async (
  event: APIGatewayProxyEvent,
  context: Context
) => {
  const userId = event?.pathParameters?.userId;
  if (!userId) {
    return {
      statusCode: HttpStatusCode.BAD_REQUEST,
      body: JSON.stringify({
        error: {
          message: `Path Parameter (userId) is missing.`,
        },
      }),
    };
  }

  const tableName = 'StreamsLimitTable';
  const logger = createLogger({
    exitOnError: false,
    defaultMeta: {
      awsRequestId: context.awsRequestId,
    },
  });

  const streamLimitRepository = new DynamoDbStreamRepository(
    tableName,
    new DynamoDB.DocumentClient()
  );
  const maxStreamsPerUser = 3;

  const streamLimitService = new StreamLimitService(
    streamLimitRepository,
    maxStreamsPerUser,
    logger
  );
  const controller = new StreamLimitController(streamLimitService, logger);
  const resposne = await controller.createStream(userId);

  return {
    statusCode: resposne.statusCode,
    body: JSON.stringify(resposne.body),
  };
};
