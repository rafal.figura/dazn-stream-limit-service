export class StreamLimitExceedError extends Error {
  constructor(maxStreamsPerUser: number) {
    super(`Open streams limit exceed. Max streams: ${maxStreamsPerUser}`);
    Object.setPrototypeOf(this, StreamLimitExceedError.prototype);
  }
}
