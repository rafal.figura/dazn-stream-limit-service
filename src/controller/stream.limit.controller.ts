import { Logger } from 'winston';
import { StreamLimitExceedError } from '../errors/stream.limit.exceed.error';
import { StreamLimitService } from '../service/stream.limit.service';
import { HttpStatusCode } from '../utils/http.status.code';
import { RequestResponse } from '../utils/request.response';

export class StreamLimitController {
  constructor(
    private readonly streamLimitService: StreamLimitService,
    private readonly logger?: Logger
  ) {}

  public async createStream(userId: string): Promise<RequestResponse> {
    try {
      const stream = await this.streamLimitService.createStream(userId);

      return {
        body: {
          stream,
        },
        statusCode: HttpStatusCode.OK,
      };
    } catch (error) {
      if (error instanceof StreamLimitExceedError) {
        return {
          statusCode: HttpStatusCode.FORBIDDEN,
          body: {
            error: {
              message: `Open streams limit exceed. Max streams: ${this.streamLimitService.getStreamsLimit()}`,
            },
          },
        };
      }

      this.logger?.log('error', error.name);
      return {
        statusCode: HttpStatusCode.BAD_GATEWAY,
        body: {
          error: {
            message: 'Could not get user streams.',
          },
        },
      };
    }
  }
}
