import { Logger } from 'winston';
import { StreamLimitExceedError } from '../errors/stream.limit.exceed.error';
import { Stream } from '../models/stream';
import { StreamRepository } from '../repository/stream.repository';

export class StreamLimitService {
  constructor(
    private readonly repository: StreamRepository,
    private readonly maxStreamsPerUser: number = 3,
    private readonly logger?: Logger
  ) {}

  public async createStream(userId: string): Promise<Stream> {
    const streams = await this.repository.getStreams(userId);

    if (streams.length >= this.maxStreamsPerUser) {
      this.logger?.log('info', `User with id: ${userId} exceed streams limit.`);
      throw new StreamLimitExceedError(this.maxStreamsPerUser);
    }

    return this.repository.createStream(userId);
  }

  public getStreamsLimit(): number {
    return this.maxStreamsPerUser;
  }
}
