import { Stream } from '../models/stream';

export interface StreamRepository {
  getStreams(userId: string): Promise<Stream[]>;
  createStream(userId: string): Promise<Stream>;
}
