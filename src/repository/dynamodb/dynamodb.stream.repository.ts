import { DynamoDB } from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';
import { Stream } from '../../models/stream';
import { StreamRepository } from '../stream.repository';

export class DynamoDbStreamRepository implements StreamRepository {
  constructor(
    private readonly tableName: string,
    private readonly dynamoDbClient: DynamoDB.DocumentClient
  ) {}

  async getStreams(userId: string): Promise<Stream[]> {
    const response = await this.dynamoDbClient
      .query({
        TableName: this.tableName,
        IndexName: 'UserIdIndex',
        KeyConditionExpression: 'userId = :userId',
        ExpressionAttributeValues: { ':userId': userId },
      })
      .promise();

    return this.mapQueryItemsToStream(response.Items);
  }

  private mapQueryItemsToStream(
    items: DynamoDB.DocumentClient.ItemList | undefined
  ): Stream[] {
    if (!items) {
      return [];
    }

    return items.flatMap<Stream>((item) => {
      if (item.streamId && item.userId) {
        return [
          {
            streamId: item.streamId,
            userId: item.userId,
          },
        ];
      }

      return [];
    });
  }

  async createStream(userId: string): Promise<Stream> {
    const stream: Stream = {
      streamId: uuidv4(),
      userId,
    };

    await this.dynamoDbClient
      .put({
        TableName: this.tableName,
        Item: stream,
      })
      .promise();

    return stream;
  }
}
