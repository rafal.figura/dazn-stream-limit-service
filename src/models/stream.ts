export interface Stream {
  userId: string;
  streamId: string;
}
