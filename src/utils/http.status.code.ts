export enum HttpStatusCode {
  OK = 200,
  BAD_REQUEST = 400,
  FORBIDDEN = 403,
  BAD_GATEWAY = 502,
}
