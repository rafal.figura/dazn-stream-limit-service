import { HttpStatusCode } from './http.status.code';

export interface RequestResponse {
  statusCode: HttpStatusCode;
  body: object;
}
