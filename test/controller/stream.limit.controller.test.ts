import { StreamLimitController } from '../../src/controller/stream.limit.controller';
import { StreamLimitExceedError } from '../../src/errors/stream.limit.exceed.error';
import { HttpStatusCode } from '../../src/utils/http.status.code';

describe('Stream Limit Controller Test', () => {
  const streamLimitService = {
    createStream: jest.fn(),
    getStreamsLimit: jest.fn(),
  };
  // @ts-expect-error mocking for tests
  const controller = new StreamLimitController(streamLimitService);

  test('Should response with stream and status code = 200', async () => {
    const stream = {
      userId: `user-id-1`,
      streamId: `stream-id-1`,
    };
    streamLimitService.createStream.mockResolvedValue(stream);

    const response = await controller.createStream(`user-id-1`);

    expect(response).toStrictEqual({
      statusCode: HttpStatusCode.OK,
      body: {
        stream,
      },
    });
  });

  test('Should response with forbidden when limit is exceed', async () => {
    const streamsLimit = 3;
    streamLimitService.createStream.mockImplementation(() => {
      throw new StreamLimitExceedError(streamsLimit);
    });
    streamLimitService.getStreamsLimit.mockReturnValue(streamsLimit);

    const response = await controller.createStream(`user-id-1`);
    expect(response.statusCode).toBe(HttpStatusCode.FORBIDDEN);
    expect(response.body).toStrictEqual({
      error: {
        message: `Open streams limit exceed. Max streams: ${streamsLimit}`,
      },
    });
  });

  test('Should response with bad gateway when other error occured', async () => {
    streamLimitService.createStream.mockImplementation(() => {
      throw new Error(`Connection to database failed`);
    });

    const response = await controller.createStream(`user-id-1`);

    expect(response.statusCode).toBe(HttpStatusCode.BAD_GATEWAY);
    expect(response.body).toStrictEqual({
      error: {
        message: `Could not get user streams.`,
      },
    });
  });
});
