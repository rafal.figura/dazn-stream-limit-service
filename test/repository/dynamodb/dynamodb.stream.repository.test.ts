import { DynamoDbStreamRepository } from '../../../src/repository/dynamodb/dynamodb.stream.repository';

describe('DynamoDB Stream Repository', () => {
  const dynamoDbDocumentMock = {
    query: jest.fn(),
    put: jest.fn(),
  };
  const tableName = 'tableName';
  const userId = 'user-id-1';
  const dynamoDbStreamRepository = new DynamoDbStreamRepository(
    tableName,
    // @ts-expect-error mocking
    dynamoDbDocumentMock
  );

  test('Should call DynamoDB put method with proper parameters', async () => {
    const clientCalls: any[] = [];

    dynamoDbDocumentMock.put.mockImplementation((params) => {
      clientCalls.push(params);
      return { promise: jest.fn() };
    });

    await dynamoDbStreamRepository.createStream(userId);

    expect(clientCalls).toHaveLength(1);
    expect(clientCalls[0].TableName).toBe(tableName);
    expect(clientCalls[0].Item.streamId).toBeDefined();
    expect(clientCalls[0].Item.userId).toBe(userId);
  });

  test('Should call DynamoDB query method with proper parameters', async () => {
    const clientCalls: any[] = [];
    const promiseMock = jest.fn();
    promiseMock.mockReturnValue({
      Items: [],
    });
    dynamoDbDocumentMock.query.mockImplementation((params) => {
      clientCalls.push(params);
      return { promise: promiseMock };
    });

    await dynamoDbStreamRepository.getStreams(userId);
    expect(clientCalls).toHaveLength(1);
    expect(clientCalls[0].TableName).toBe(tableName);
    expect(clientCalls[0].IndexName).toBe(`UserIdIndex`);
    expect(clientCalls[0].ExpressionAttributeValues).toStrictEqual({
      ':userId': 'user-id-1',
    });
  });

  test('Should map reposne items', async () => {
    const clientCalls: any[] = [];
    const itemsResponse = [
      {
        streamId: 'stream-id-1',
        userId: 'user-id-1',
      },
    ];
    const promiseMock = jest.fn();
    promiseMock.mockReturnValue({
      Items: itemsResponse,
    });
    dynamoDbDocumentMock.query.mockImplementation((params) => {
      clientCalls.push(params);
      return { promise: promiseMock };
    });

    const response = await dynamoDbStreamRepository.getStreams(userId);
    expect(clientCalls).toHaveLength(1);
    expect(clientCalls[0].TableName).toBe(tableName);
    expect(clientCalls[0].IndexName).toBe(`UserIdIndex`);
    expect(clientCalls[0].ExpressionAttributeValues).toStrictEqual({
      ':userId': 'user-id-1',
    });

    expect(response).toHaveLength(1);
    expect(response).toStrictEqual(itemsResponse);
  });
});
