import { StreamLimitExceedError } from '../../src/errors/stream.limit.exceed.error';
import { StreamLimitService } from '../../src/service/stream.limit.service';

describe('Stream Limit Service Test', () => {
  test('Should create new stream', async () => {
    const streamLimit = 3;
    const mockStreamRepository = {
      getStreams: jest.fn(),
      createStream: jest.fn(),
    };
    mockStreamRepository.getStreams.mockResolvedValue([]);
    mockStreamRepository.createStream.mockImplementation((userId) => ({
      userId,
      streamId: 'stream-id-1',
    }));

    const streamLimitService = new StreamLimitService(
      mockStreamRepository,
      streamLimit
    );
    const response = await streamLimitService.createStream(`user-id-1`);

    expect(response).toStrictEqual({
      userId: `user-id-1`,
      streamId: `stream-id-1`,
    });
  });

  test('Should throw exception when limit exceed', async () => {
    expect.assertions(1);

    const streamLimit = 3;
    const mockStreamRepository = {
      getStreams: jest.fn(),
      createStream: jest.fn(),
    };
    mockStreamRepository.getStreams.mockResolvedValue([
      { userId: `user-id-1`, streamId: `stream-id-1` },
      { userId: `user-id-1`, streamId: `stream-id-2` },
      { userId: `user-id-1`, streamId: `stream-id-3` },
    ]);

    const streamLimitService = new StreamLimitService(
      mockStreamRepository,
      streamLimit
    );

    try {
      await streamLimitService.createStream(`user-id-1`);
    } catch (error) {
      expect(error).toBeInstanceOf(StreamLimitExceedError);
    }
  });
});
