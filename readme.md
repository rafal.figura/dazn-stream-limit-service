# DAZN coding challenge

My approach for building a service in Node.js that exposes an API which can be consumed from any client.

## stream-limit-service

It uses serverless framework with dynamodb.
Scalability is achieved by taking advantage of AWS Lambda. In case of throttling you can change read/write capacity units in `serverless.yml` settings.

### How to deploy

Create organization, application and service in `app.serverless.com` and edit `org`, `app` and `service` in `serverless.yml`.

```
  npm install
  sls login
  sls deploy
```

### Testing

Run `npm test` to test application. Use `npm run test:coverage` to test with coverage.

### Active endpoint

[https://3vjob3skzh.execute-api.eu-west-1.amazonaws.com/dev/users/{userId}/streams](https://3vjob3skzh.execute-api.eu-west-1.amazonaws.com/dev/users/{userId}/streams)

Replace `{userId}` with your userId. It will be converted to string.
Use method `POST`

```
curl -X POST https://3vjob3skzh.execute-api.eu-west-1.amazonaws.com/dev/users/user-identifier-1/streams
```

## Further improvements

- Use enviroments provided by serverless framwork
- Instead of static dynamodb table name use generated one
- Add source map (for better error catching)
- Connecto logs to CloudWatch
- Add more log levels
- E2E tests
